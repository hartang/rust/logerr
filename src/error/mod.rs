//! # Default logging implementations
//!
//! Implements default logging for common error types.

#[cfg(all(feature = "generic", feature = "anyhow"))]
compile_error!("feature 'generic' and feature 'anyhow' cannot be used at the same time");

#[cfg(feature = "anyhow")]
mod anyhow;
#[cfg(feature = "generic")]
mod generic;
