//! # Error logging for `anyhow::Result<_>`
//!
//! > Requires the `anyhow` feature!
//!
//! Uses the default debug formatting for [`anyhow::Result`][anyhow::Result].
use crate::LoggableError;

impl<T> LoggableError<T> for anyhow::Result<T> {
    fn print_error<F: Fn(&str)>(self, fun: F) -> Self {
        if let Err(ref err) = self {
            fun(&format!("{:?}", err));
        }
        self
    }
}
