# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.2.0] - 2023-05-07
### Added
- feature: Add partial caller tracking
- Throw a compiler error when using mutually eclusive features

### Changed
- The `generic` feature now uses the `std::error::Error` trait instead of
  `Debug`

### Removed
- `std_error` module is no longer available

## [0.1.0] - 2022-08-20
- Initial release
